erlang-p1-yaml (1.0.32-2) unstable; urgency=medium

  * Updated debian/watch

 -- Philipp Huebner <debalance@debian.org>  Mon, 20 Dec 2021 19:39:33 +0100

erlang-p1-yaml (1.0.32-1) unstable; urgency=medium

  * New upstream version 1.0.32
  * Updated Standards-Version: 4.6.0 (no changes needed)
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 29 Aug 2021 22:01:10 +0200

erlang-p1-yaml (1.0.30-2) unstable; urgency=medium

  * Corrected Multi-Arch setting to "allowed"

 -- Philipp Huebner <debalance@debian.org>  Sun, 31 Jan 2021 19:31:27 +0100

erlang-p1-yaml (1.0.30-1) unstable; urgency=medium

  * New upstream version 1.0.30
  * Added 'Multi-Arch: same' in debian/control
  * Updated years in debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 30 Jan 2021 19:05:18 +0100

erlang-p1-yaml (1.0.29-1) unstable; urgency=medium

  * New upstream version 1.0.29
  * Updated Standards-Version: 4.5.1 (no changes needed)
  * Updated Erlang dependencies
  * Updated version of debian/watch: 4
  * Updated debhelper compat version: 13
  * Updated lintian overrides
  * Added debian/erlang-p1-yaml.install

 -- Philipp Huebner <debalance@debian.org>  Sat, 26 Dec 2020 00:34:28 +0100

erlang-p1-yaml (1.0.27-1) unstable; urgency=medium

  * New upstream version 1.0.27
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 02 Aug 2020 17:44:56 +0200

erlang-p1-yaml (1.0.26-1) unstable; urgency=medium

  * New upstream version 1.0.26
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jul 2020 22:23:22 +0200

erlang-p1-yaml (1.0.24-1) unstable; urgency=medium

  * New upstream version 1.0.24

 -- Philipp Huebner <debalance@debian.org>  Sat, 18 Apr 2020 09:18:13 +0200

erlang-p1-yaml (1.0.23-1) unstable; urgency=medium

  * New upstream version 1.0.23
  * Updated Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Wed, 18 Mar 2020 13:01:28 +0100

erlang-p1-yaml (1.0.22-1) unstable; urgency=medium

  * New upstream version 1.0.22
  * Updated Standards-Version: 4.5.0 (no changes needed)
  * Updated Erlang dependencies
  * Updated years in debian/copyright
  * Rules-Requires-Root: no

 -- Philipp Huebner <debalance@debian.org>  Wed, 05 Feb 2020 19:58:39 +0100

erlang-p1-yaml (1.0.21-1) unstable; urgency=medium

  * New upstream version 1.0.21
  * Updated Standards-Version: 4.4.1 (no changes needed)
  * Updated debhelper compat version: 12
  * Fixed typo in long package description.

 -- Philipp Huebner <debalance@debian.org>  Mon, 11 Nov 2019 14:35:00 +0100

erlang-p1-yaml (1.0.20-1) unstable; urgency=medium

  * New upstream version 1.0.20
  * Updated Erlang dependencies
  * Added upstream patch to fix failing unit test

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 Aug 2019 14:46:19 +0200

erlang-p1-yaml (1.0.19-1) unstable; urgency=medium

  * New upstream version 1.0.19
  * Updated Erlang dependencies
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.4.0 (no changes needed)
  * debian/rules: export ERL_COMPILER_OPTIONS=deterministic
  * Updated debhelper compat version: 12

 -- Philipp Huebner <debalance@debian.org>  Wed, 24 Jul 2019 13:01:03 +0200

erlang-p1-yaml (1.0.17-1) unstable; urgency=medium

  * New upstream version 1.0.17
  * Enabled DH_VERBOSE in debian/rules
  * Updated Standards-Version: 4.2.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 07 Oct 2018 14:58:11 +0200

erlang-p1-yaml (1.0.15-1) unstable; urgency=medium

  * New upstream version 1.0.15
  * Added debian/upstream/metadata

 -- Philipp Huebner <debalance@debian.org>  Wed, 04 Jul 2018 16:25:15 +0200

erlang-p1-yaml (1.0.14-1) unstable; urgency=medium

  * New upstream version 1.0.14
  * Updated Standards-Version: 4.1.4 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Wed, 09 May 2018 17:10:22 +0200

erlang-p1-yaml (1.0.13-1) unstable; urgency=medium

  * New upstream version 1.0.13
  * Use secure copyright format uri in debian/copyright
  * Switched to debhelper 11

 -- Philipp Huebner <debalance@debian.org>  Wed, 28 Mar 2018 00:17:34 +0200

erlang-p1-yaml (1.0.12-2) unstable; urgency=medium

  * Set Maintainer: Ejabberd Packaging Team <ejabberd@packages.debian.org>
  * Set Uploaders: Philipp Huebner <debalance@debian.org>
  * Updated Vcs-* fields in debian/control for salsa.debian.org
  * Updated years in debian/copyright
  * Updated Standards-Version: 4.1.3 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Thu, 04 Jan 2018 11:38:04 +0100

erlang-p1-yaml (1.0.12-1) unstable; urgency=medium

  * New upstream version 1.0.12
  * (Build-)Depend on erlang-base (>= 1:19.2)
  * Updated Standards-Version: 4.1.1 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Mon, 27 Nov 2017 20:11:06 +0100

erlang-p1-yaml (1.0.10-1) unstable; urgency=medium

  * New upstream version 1.0.10
  * Updated Standards-Version: 4.0.0 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Tue, 11 Jul 2017 17:21:01 +0200

erlang-p1-yaml (1.0.9-1~exp1) experimental; urgency=medium

  * New upstream version 1.0.9
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sat, 15 Apr 2017 13:44:01 +0200

erlang-p1-yaml (1.0.6-2) unstable; urgency=medium

  * Added erlang-base to Build-Depends

 -- Philipp Huebner <debalance@debian.org>  Wed, 05 Oct 2016 15:58:00 +0200

erlang-p1-yaml (1.0.6-1) unstable; urgency=medium

  * New upstream version 1.0.6
  * (Build-)Depend on erlang-p1-utils (>= 1.0.5)

 -- Philipp Huebner <debalance@debian.org>  Fri, 16 Sep 2016 14:56:52 +0200

erlang-p1-yaml (1.0.5-1) unstable; urgency=medium

  * Imported Upstream version 1.0.5

 -- Philipp Huebner <debalance@debian.org>  Fri, 05 Aug 2016 20:26:38 +0200

erlang-p1-yaml (1.0.4-1) unstable; urgency=medium

  * Imported Upstream version 1.0.4

 -- Philipp Huebner <debalance@debian.org>  Sun, 03 Jul 2016 16:58:42 +0200

erlang-p1-yaml (1.0.3-2) unstable; urgency=medium

  * Improved debian/watch
  * Updated Standards-Version: 3.9.8 (no changes needed)

 -- Philipp Huebner <debalance@debian.org>  Sun, 26 Jun 2016 14:13:33 +0200

erlang-p1-yaml (1.0.3-1) unstable; urgency=medium

  * Imported Upstream version 1.0.3
  * Enabled hardening

 -- Philipp Huebner <debalance@debian.org>  Wed, 30 Mar 2016 20:02:54 +0200

erlang-p1-yaml (1.0.2-1) unstable; urgency=medium

  * ProcessOne renamed p1_yaml to fast_yaml
  * Imported Upstream version 1.0.2
  * Updated debian/copyright

 -- Philipp Huebner <debalance@debian.org>  Sun, 14 Feb 2016 15:22:28 +0100

erlang-p1-yaml (1.0.1-1) unstable; urgency=medium

  * Imported Upstream version 1.0.1
  * Updated Standards-Version: 3.9.7 (no changes needed)
  * Updated Vcs-* fields in debian/control

 -- Philipp Huebner <debalance@debian.org>  Wed, 03 Feb 2016 10:03:47 +0100

erlang-p1-yaml (1.0.0-1) unstable; urgency=medium

  * Imported Upstream version 1.0.0
  * Updated debian/copyright
  * Updated debian/watch

 -- Philipp Huebner <debalance@debian.org>  Sat, 16 Jan 2016 12:46:22 +0100

erlang-p1-yaml (0.2015.10.07-1) unstable; urgency=medium

  * Imported Upstream version 0.2015.10.07

 -- Philipp Huebner <debalance@debian.org>  Sat, 31 Oct 2015 16:37:32 +0100

erlang-p1-yaml (0.2015.09.25-1) unstable; urgency=medium

  * Imported Upstream version 0.2015.09.25

 -- Philipp Huebner <debalance@debian.org>  Sun, 04 Oct 2015 10:27:36 +0200

erlang-p1-yaml (0.2014.06.11-3) unstable; urgency=medium

  * Enabled eunit

 -- Philipp Huebner <debalance@debian.org>  Mon, 17 Aug 2015 17:46:59 +0200

erlang-p1-yaml (0.2014.06.11-2) unstable; urgency=medium

  * Updated Standards-Version: 3.9.6 (no changes needed)
  * Updated years in debian/copyright
  * Added Vcs links to debian/control

 -- Philipp Huebner <debalance@debian.org>  Sat, 02 May 2015 19:02:20 +0200

erlang-p1-yaml (0.2014.06.11-1) unstable; urgency=medium

  * Imported Upstream version 0.2014.06.11

 -- Philipp Huebner <debalance@debian.org>  Thu, 10 Jul 2014 17:48:43 +0200

erlang-p1-yaml (0.2013.12.09-2) unstable; urgency=medium

  * Added correct Erlang dependencies

 -- Philipp Huebner <debalance@debian.org>  Sat, 10 May 2014 12:56:15 +0200

erlang-p1-yaml (0.2013.12.09-1) unstable; urgency=low

  * Initial release, see #744885 for background

 -- Philipp Huebner <debalance@debian.org>  Wed, 16 Apr 2014 18:34:06 +0200
